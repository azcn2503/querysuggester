# querySuggester

Autocompletion for your querySelectorAll!

### Introduction

This project was born out of a need to provide users (and other developers) with autocomplete suggestions when typing in CSS selector queries using `document.querySelector` and `document.querySelectorAll`. These existing methods are perfect when you know exactly what you want, but if you only have a vague idea and want to throw around some different ideas, you're often left with `null` or `[]` and that makes me very sad. This situation might come around quite often when working with PhantomJS, Selenium, and other situations whereby you are loading a page without a GUI.

Now you can use querySuggester! It works by itself (just inject it in to a page, it has no dependencies) or it can work as part of some other application. I'm personally using it for another up-and-coming Node.js project that I would like to share with the world some time!

### How to use

You can copy and paste the code in to your browsers development console and use the `suggest` function as if you were passing in an incomplete querySelector string. It takes an optional `mode` argument which defaults to `"native"` so you can use the result immediately, but can also accept `"array"` or `"object"` so you can get a simple list of matching element names, id's and classes.

### Examples

    suggest('bo', 'array');
    // returns [ "BODY.ui-layout-container" ]
    
---

    suggest('body di', 'array');
    // returns [ "DIV#MathJax_Message", "DIV.navbar.navbar-default.ui-layout-north.ui-layout-pane.ui-layout-pane-north", "DIV.navbar-inner", ... ]
    
---

    suggest('a>', 'array');
    // returns [ "I.icon-resize-full", "I.icon-hdd", "I.icon-provider-gdrive", ... ]
    
### Behaviour

It works by translating your querySelector in to the most vague form possible, with some added magic for incomplete tag names, since there is no native way in JavaScript to search for partial tag names (I think jQuery has some special method for this, but I did not want to be dependant on that library).

So, when you type in something like: `a.` it says "I am looking for all `a` tags that have a class attribute" - your query is effectively changed to `*[_tagName*=a][class]`.

If you type in `a.b` it says "I am looking for all `a` tags that have a class attribute containing `b`" - your query is effectively changed to `*[_tagName*=a][class*=b]`

If it's `div > d`, your query will convert to `*[_tagName*=div] > *[_tagName*=d]`.

### Limitations

It is currently not possible to use pseudo-selectors, attribute selectors, and sibling selectors. Though I may add them in the future, or somebody else can!

### What it can do

It can work with incomplete tags, incomplete ids, multiple incomplete classes, descendants and direct descendants.