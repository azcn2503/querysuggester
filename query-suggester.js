var suggest = function(selector, mode, inContext) {
  
  var self = this;
  
  this.returnNodes = function(nodes) {
    
    if(self.mode == 'native') {
      // Return an array of native elements
      var arr = [];
      for(var i in nodes) {
        if(typeof(nodes[i]) !== 'object') { continue; }
        arr.push(nodes[i]);
      }
      return arr;
    }
    
    if(self.mode == 'array' || self.mode == 'object') { 
      var obj = {};
      for(var i in nodes) {
        if(!nodes[i] || !nodes[i].tagName) {
          continue;
        }
        var tagString = self.inContext ? self.context == 'tag' ? nodes[i].tagName : '' : nodes[i].tagName;
        var id = self.inContext ? self.context == 'id' ? nodes[i].id : '' : nodes[i].id;
        id = id ? '#' + id : id;
        var classString = '';
        if(self.inContext && self.context == 'class') {
          var classes = nodes[i].className.split(' ');
          for(var i in classes) {
            if(classes[i] == '') { continue; }
            classString += '.' + classes[i];
          }
        }
        var selector = tagString + id + classString;
        obj[selector] = true;
      }
      if(self.mode == 'object') { return obj; } // Return a simple object of selectors
      var arr = [];
      for(var i in obj) {
        arr.push(i);
      }
      return arr; // Return a simple array of selectors
    }
    
  };
  
  this.mode = mode || 'native';
  this.inContext = inContext || false;
  this.context = 'tag';
  var tagHint = null;
  var tagHints = [];
  
  var contexts = {
    '#': 'id',
    '.': 'class'
  };
  
  // get context when in context mode
  var lastSeparator = selector.match(/([> #.])(?=[^> #.]*$)/);
  if(this.inContext && lastSeparator) {
    lastSeparator = lastSeparator[0];
    if(contexts[lastSeparator]) { this.context = contexts[lastSeparator]; }
  }
  
  // add _tagName attribute to all elements (make tag names searchable)
  for(var i = 0, els = document.querySelectorAll('*'), elsLength = els.length; i < elsLength; i++) {
    if(!els[i] || !els[i].tagName) { continue; }
    els[i].setAttribute('_tagName', els[i].tagName.toLowerCase());
  }
  
  // generate the new query
  var newSelector = [];
  selector.split(',').forEach(function(el, i) {
    el = el.replace(/(^|[> #.])([^> #.]*)/gi, function(match, p1, p2) {
      if(p1 != '.' && p1 != '#') {
        if(p2 == '' || p2 == '*') { return p1 + '*'; }
        return p1 + '[_tagName^=' + p2 + ']';
      }
      return p1 + p2;
    }).replace(/([#.])([a-z0-9\-_:]*)/gi, function(match, p1, p2) {
      var attr = contexts[p1];
      if(p2 == '') { return '[' + attr + ']'; }
      return '[' + attr + '*=' + p2 + ']';
    });
    newSelector.push(el);
  });
  newSelector = newSelector.join(',');
  
  // execute the query on the current page
  var nodes = document.querySelectorAll(newSelector);
  
  return this.returnNodes(nodes);
  
}